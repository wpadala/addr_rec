#include <iostream>
#include "libpostal/src/libpostal.h"
#include <memory>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>

using namespace std;


void recognize(const char* str)
{
    int labels_count = 0;
    std::vector<std::pair<std::string, std::string> > elements;
    libpostal_address_parser_options_t options = libpostal_get_address_parser_default_options();
    libpostal_address_parser_response_t* address = libpostal_parse_address(const_cast<char*>(str), options);
    for(size_t i = 0; i < address->num_components; i++)
    {
        std::string road = "road", house_number = "house_number", city = "city", postcode = "postcode";
        if(road.compare(address->labels[i]) == 0)
        {
            labels_count++;
            elements.push_back(std::make_pair<std::string, std::string>("Ulica", address->components[i]));
        }
        else if(house_number.compare(address->labels[i]) == 0)
        {
            labels_count++;
            elements.push_back(std::make_pair<std::string, std::string>("Numer domu", address->components[i]));

        }
        else if(postcode.compare(address->labels[i]))
        {
            labels_count++;
            elements.push_back(std::make_pair<std::string, std::string>("Kod pocztowy", address->components[i]));

        }
        else if(city.compare(address->labels[i]))
        {
            labels_count++;
            elements.push_back(std::make_pair<std::string, std::string>("Miasto", address->components[i]));

        }
    }
    if(labels_count >= 4)
    {
        std::cout << "ZNALEZIONO PEŁNY ADRES \n";
    }
    for (size_t i = 0; i < address->num_components; i++) {
        printf("%s: %s\n", address->labels[i], address->components[i]);
    }


    libpostal_address_parser_response_destroy(address);

}

int main()
{
    if(!libpostal_setup() || !libpostal_setup_parser())
    {
        fprintf(stderr, "Nie zainstalowano biblioteki libpostal. \n");

    }
    std::string input;
    std::cout << "Podaj ciąg wejściowy lub wpisz q aby wyjść:\n";
    std::getline(std::cin, input);
    while(input != "q")
    {
        recognize(input.c_str());
        std::cout << "Podaj ciąg wejściowy lub wpisz q aby wyjść:\n";
        input = "";
        std::getline(std::cin, input);
    }
    libpostal_teardown();
    libpostal_teardown_parser();
}